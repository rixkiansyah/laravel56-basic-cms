let mix = require('laravel-mix');

/*
 | Modified for direct access writing if
 | we use the Vue.js or somewhat js
 */

mix.setPublicPath(path.normalize('../public'));
mix.js('resources/assets/js/app.js', '../public/js/app.js')
   .sass('resources/assets/sass/app.scss', '../public/sass/app.scss');

